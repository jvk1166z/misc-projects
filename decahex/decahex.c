/*
This program's design goals are as follows:

1 Take user input from the commmandline
2 Determine mode, eg. ./decahex -d 0xF
    switch -d for dec to hex and switch -h for hex to dec.
3 Determine if the user entered the correct numeral system. Throw error if incorrect.
4 Convert to the proper system
5 Return ther result to the user
6 Terminate
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define TRUE 1
#define FALSE 0
#define DEC 2
#define HEX 3

int checkargs(int argc, char** argv, long long* pdec, char** phex);
int decnumber(char hex);
char hexdigit(int number);
void decahex(long long pdec, char* phex);
void hexadec(char* phex, long long* pdec);

int main(int argc, char** argv)
{
    int argres = 0;

    long long dec = 0;
    char hex[17] = {0};
    char* phexbuffer = NULL;


    if(FALSE != (argres = checkargs(argc, argv, &dec, &phexbuffer)))
    {
        switch(argres)
        {
            case DEC:
                decahex(dec, hex);
                printf("0x%s", hex);
                return 0;

            case HEX:
                hexadec(phexbuffer, &dec);
                printf("%I64d", dec);
                return 0;
        }

    }

    return FALSE;
}

int checkargs(int argc, char** argv, long long* pdec, char** phex)
{
    if(argc > 3)
    {
        fprintf(stderr, "You entered too few arguments.");
        return FALSE;
    }

    if(argc < 3)
    {
        fprintf(stderr, "You entered too few arguments.");
        return FALSE;
    }

    if(argc == 3)
    {
        if(strcmp(argv[1], "-d") == 0)
        {
            *pdec = _atoi64(argv[2]);
            return DEC;
        }

        else if(strcmp(argv[1], "-h") == 0)
        {
            *phex = argv[2];
            return HEX;
        }

        else
        {
            fprintf(stderr, "Error, incorrect or no switch used.");
            return FALSE;
        }
    }

    return TRUE;
}

void decahex(long long dec, char* phex)
{
    char buffer[17] = {0};
    int counter = 0;

    do
    {
        int value = 0;

        value = dec & 0xF;
        buffer[counter] = hexdigit(value);

        dec = dec >> 4;
        counter++;
    }
    while(dec != 0);

    for(int x = counter - 1; x >= 0; x--)
    {
        *phex = buffer[x];
        phex++;
    }

    *phex = 0x00;
}

void hexadec(char* phex, long long* pdec)
{
    *pdec = 0;

    if(phex[0] == '0' && (phex[1] == 'x' || phex[1] == 'X'))
    {
        phex = phex + 2;
    }

    while(*phex != 0x00)
    {
        int decnumberresult = decnumber(*phex);

        if(decnumberresult > 15)
        {
            fprintf(stderr, "wrong\n");
            break;
        }

        *pdec = *pdec << 4;
        *pdec = *pdec + decnumberresult;
        phex++;
    }
}

char hexdigit(int number)
{
    const char* pdigits = "0123456789ABCDEF";
    //printf("%d\n", number);
    return pdigits[number];
}

int decnumber(char hex)
{
    hex = toupper(hex);

    const char* pdigits = "0123456789ABCDEF";
    int counter = 0;

    while(*pdigits != hex && *pdigits != 0x00)
    {
        counter++;
        pdigits++;
    }

    //printf("%d %c\n", counter, hex);
    return counter;
}
