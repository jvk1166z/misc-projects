/*
This is another practise program I'm doing for my K&R C book.
It is a simple program, it only determines the range of a given variable type.
*/

#include <stdio.h>
#include <math.h>

double calcsizeofsigned(int bytes);
int mystrlen(const char* ptext);

int main()
{
    printf("size of char: %f\n", calcsizeofsigned(sizeof(char)));
    printf("size of short: %f\n", calcsizeofsigned(sizeof(short)));
    printf("size of int: %f\n", calcsizeofsigned(sizeof(int)));

    return 0;
}

double calcsizeofsigned(int bytes)
{
    const double base = 2.0;
    const double bpb = 8.0;
    double res = 0;

    res = pow(base,(double)bytes * bpb - 1);

    return res - 1;
}

int mystrlen(const char* ptext)
{
    int counter = 0;

    while(*ptext != 0x00)
    {
        counter++;
        ptext++;
    }

    return counter;
}
