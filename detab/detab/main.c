/*
I decided to save space and document all of what this program does in the source for it. This is a extremely overbuilt version of the detab program suggested as a challenge at the end of
chapter 2 in the second edition of K&R C. The program on its own was dead simple and just a test of the concepts learned in that chapter of the book, so I decided to add some difficulty
and improve upon it by adding some features.

The original program was only intended to take any input and spit it out with the tabs replaced with a set number of spaces, I decided to modify it to take command line arguments and allow
the user to specify which character to use and how many of it per each tab. I have been trying to limit myself to only using stdio, forcing me to write most of the functions that would be
provided by libraries such as stdlib in order to fully understand how the language works. Though later I'll probably end up using stdlib for malloc() but for now it's fine.

It may not be the most special or advanced of programs, but I haven't written much else in the past year or so and since I've been learning C I thought it was a perfect fit to send in.
*/

#include <stdio.h>

#ifdef linux
    #define USAGE "./detab [characters per tab] [\x22character to use\x22]"
#endif // linux

#ifdef _WIN32
    #define USAGE "detab.exe [characters per tab] [\x22character to use\x22]"
#endif // _WIN32

#define TAB 0x09
#define SPC 0x20
#define LF 0x0A
#define CR 0x0D

char* mystrchr(char* pfrom, char character);
char* mystrrchr(char* pfrom, char character);
void detab(int tabLen, char input, char *argv[], int argc);
void myatoi(char *argv, int* pTabLen);
void mystrcpy(char* pfrom, char* pto);
void mystrcat(char* pfrom, char* pto);
int parseargc(int argc);
int mystrlen(char* pText);

int main(int argc, char *argv[])
{
    if(parseargc(argc) == 0)
    {
        char input = 0;
        int tabLen = 0;

        myatoi(argv[1], &tabLen); // Uses my own ATOI function to convert the second commandline argument into an integer usable by the numt of the program.

        while((input = getchar()) != EOF)
            detab(tabLen, input, argv, argc);

        return 0;
    }

    return 1;
}

int parseargc(int argc) // Makes sure commandline input is the right length.
{
    if(argc > 3)
    {
        printf("Usage: %s", USAGE);
        return 1;
    }
    else if(argc < 2)
    {
        printf("Usage: %s", USAGE);
        return 1;
    }

    return 0;
}

void detab(int tabLen, char input, char *argv[], int argc) //  Actual detab function.
{
    int tabChar = 0;
    int x = 0;

    if(argc > 2) // Determines if the user specified a character to replace tabs with.
        tabChar = *argv[2];

    else // If the user has not specified anything, use the default.
        tabChar = SPC;

    while(input == TAB) // The actual detab function.
    {
        for(x = 0; x < tabLen; x++)
            putchar(tabChar);

        input = getchar();
    }

    putchar(input);
}

void myatoi(char *argv, int* pTabLen) // I wrote this because I didnt want to needlessly import stdlib.h just for atoi.
{
    int num = 0;
    int sign = 1;

    if(*argv == 0x2d) // This functionality is completely and utterly useless as a negitive number of characters to replace a tab is impossible, but it is kinda fun to see it freak out when you input a negitive.
        sign = -1;

    while(*argv != 0x00) // This is the actual atoi loop, its very simple and relies on the same formula as the atoi() function in stdlib.h, I used pointers here as its whats done in atoi() and I was tyring to learn how to use them effectively anyways.
    {
        num = num * 10 + *argv - 0x30;
        argv++;
    }

    *pTabLen = sign*num;
}

// Passed this point these have nothing to do with the detab program, I just wrote them myself to practise using pointers.
// All of them are string traversal just like the atoi function.

int mystrlen(char* pText) // Dead simple, first one I wrote to test out pointers.
{
    int counter = 0;

    while(*pText != 0x00)
    {
        counter++;
        pText++;
    }

    return counter;
}

void mystrcpy(char* pfrom, char* pto) // Copies one string to another.
{
    while(*pfrom != 0x00)
    {
        *pto = *pfrom;
        pto++;
        pfrom++;
    }
    *pto = 0;
}

void mystrcat(char* pfrom, char* pto) // Glues one string to the end of another.
{
    while(*pto != 0x00)
        pto++;

    mystrcpy(pfrom, pto);
}

char* mystrchr(char* pfrom, char character) // Searches for a specified character in a specified string and returns it if found.
{
    while(*pfrom != 0x00 && *pfrom != character)
        pfrom++;

    if(*pfrom == character)
        return pfrom;

    return NULL;
}

char* mystrrchr(char* pfrom, char character) // Does the same as mystrchr() except in reverse.
{
    char* pstart = pfrom;
    while(*pfrom != 0x00)
        pfrom++;

    while(*pfrom != character && pfrom >= pstart)
        pfrom--;

    if(pfrom >= pstart)
        return pfrom;

    return NULL;
}
