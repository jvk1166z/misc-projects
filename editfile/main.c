/*
Trying to learn how to open and use files.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define TRUE 1
#define FALSE 0

int loadfile(char* pfilename, char* buffer[], int* plinecount);
void printfile(char* buffer[], int linecount);
void sortfile(char* buffer[], int linecount, int direction);

int main()
{
    char* buffer[4096];
    char cwd[MAX_PATH];
    int lines = 4096;

    if(getcwd(cwd, sizeof(cwd)))
    {
        printf("cwd = %s\n", cwd);
    }

    if(loadfile("text.txt", buffer, &lines))
    {
        sortfile(buffer, lines, -1);
        printfile(buffer, lines);
    }
    else
    {
        fprintf(stderr, "something went wrong sorry pls call microsoft 1800-india\n");
    }

    return 0;
}

int loadfile(char* pfilename, char* buffer[], int* plinecount)
{
    int lines = 0;
    FILE* pfile = NULL;
    char tmpbuffer[1024];

    pfile = fopen(pfilename, "r");

    if(pfile == NULL)
    {
        return FALSE;
    }

    while(!feof(pfile) &&  lines < *plinecount)
    {
        const char* line = fgets(tmpbuffer, sizeof(tmpbuffer), pfile);

        if(line == NULL)
        {
            break;
        }

        int length = strlen(line);
        char* pbuffer = malloc(length+1);

        strcpy(pbuffer, line);
        buffer[lines] = pbuffer;

        lines++;
    }

    fclose(pfile);
    *plinecount = lines;

    return TRUE;
}

void printfile(char* buffer[], int linecount)
{
    for(int index = 0; index < linecount; index++)
    {
        printf("%s\n", buffer[index]);
    }
}

void sortfile(char* buffer[], int linecount, int direction)
{
    int swaps = 0;

    do
    {
        swaps = 0;

        for(int ln = 0; ln < linecount - 1; ln++)
        {
            int cmp = strcmpi(buffer[ln], buffer[ln + 1]);

            if(cmp == direction)
            {
                char* templn = buffer[ln];

                buffer[ln] = buffer[ln + 1];
                buffer[ln + 1] = templn;

                swaps++;
            }
        }
    }
    while(swaps != 0);
}
